/*
 * These are globally available directives in any template
 */

// Angular 2 Core
//import {
//  PLATFORM_DIRECTIVES
//}                           from '@angular/core';
// Angular 2 Router

// application_directives: directives that are global through out the application
const APPLICATION_DIRECTIVES = [
  //...ROUTER_DIRECTIVES
];

export const DIRECTIVES = [
  //{provide: PLATFORM_DIRECTIVES, multi: true, useValue: APPLICATION_DIRECTIVES }
];
